import Vue from 'vue';
import App from './App.vue';
import router from './router';

import vueSmoothScroll from 'vue2-smooth-scroll';

import {
  library
} from '@fortawesome/fontawesome-svg-core';
import {
  faHotel,
  faUtensils,
  faDumbbell,
  faPhone,
  faEnvelope,
  faBookOpen,
  faUniversity,
  faBookReader,
  faPencilAlt
} from '@fortawesome/free-solid-svg-icons';
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome';

library.add(
  faHotel,
  faUtensils,
  faDumbbell,
  faPhone,
  faEnvelope,
  faBookOpen, faUniversity,
  faBookReader,
  faPencilAlt);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(vueSmoothScroll);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');