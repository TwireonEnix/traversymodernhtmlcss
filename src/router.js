import Vue from 'vue';
import Router from 'vue-router';
import Basics from './views/Basics';
import CssBasics from './views/CssBasics';
import ProjectOne from './views/ProjectOne';
import ProjectTwo from './views/ProjectTwo';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/html',
    name: 'home',
    component: Basics
  }, {
    path: '/css',
    name: 'css',
    component: CssBasics
  }, {
    path: '/one',
    component: ProjectOne,
    children: [{
      path: '',
      name: 'ponehome',
      component: () => import('./components/first-project/hHome')
    }, {
      path: 'about',
      name: 'poneabout',
      component: () => import('./components/first-project/hAbout')
    }, {
      path: 'contact',
      name: 'ponecontact',
      component: () => import('./components/first-project/hContact')
    }]
  }, {
    path: '/two',
    name: 'ptwohome',
    meta: {
      title: 'Edge Ledger | Financial & Investment'
    },
    component: ProjectTwo
  }]
});